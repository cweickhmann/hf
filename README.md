# Hochfrequenztechnik

## Wie benutze ich es?

Der einfachste Weg (online) besteht darin, das Notebook in MyBinder zu öffnen:
* [Wellenausbreitung in 5G-Antennen (Arrays)](https://mybinder.org/v2/gl/cweickhmann%2Fhf/HEAD)

Warten Sie bis die Instanz geladen ist. Das kann eine oder zwei Minuten
dauern.

Es öffnet sich dann eine JupyterLab-Instanz, in deren Dateiliste (links) Sie
die Datei "Wellenausbreitung..." finden (siehe Screenshot).
Doppel-klicken Sie darauf, um sie zu öffnen.
Nach einigen Sekunden dürfte sie fertig geladen sein.

Die interaktiven Teile des Notebooks können von oben nach unten
(nacheinander!) ausgeführt werden. Klicken Sie dazu auf die Code-Blöcke und
drücken Sie <kbd>SHIFT</kbd>+<kbd>ENTER</kbd>.

![](img/binder_startpage.png)


## Runterladen, lokal laufen lassen

Man kann das Repository auch herunterladen.
Dazu benötigt man lokal Git und eine Python-Installation
mit Jupyter Notebook und Jupyter Widgets.
